# USPS-Netlify

Get USPS tracking info, using a Netlify Lambda function.

## Requirements

1. Get a USPS API user ID - visit [USPS Web Tools](https://www.usps.com/business/web-tools-apis/welcome.htm) and register.
1. Add user ID as environment varaible (through Netlify interface, locally in a .env file) named `USERID`.
