require('dotenv').config();
const {USPS} = require('./util/usps');

const usps = new USPS(process.env.USERID);

const getLabels = str => {
  if(!str || !str.length) {
    return null;
  }
  else if(str.includes(',')) {
    return str.split(',');
  }
  else {
    return str;
  }
};

exports.handler = async (event, context) => {
  try {
    let headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    const labels = getLabels(event.queryStringParameters.labels);
    if(!labels) {
      throw new Error('couldn\'t find tracking label/s. please use the \'label\' parameter');
    }
    else {
      console.log('checking tracking for', labels);
    }
    let delivery;
    if(event.queryStringParameters.full === '1' || event.queryStringParameters.full === 'true') {
      delivery = await usps.getFullTrackingInfo(labels);
    }
    else {
      delivery = await usps.getDeliveryDates(labels);
    }
    let body = JSON.stringify(delivery);
    return {statusCode: 200, headers, body};
  }
  catch(err) {
    console.log(err.message);
    return {statusCode: 500, body: err.message};
  }
};
