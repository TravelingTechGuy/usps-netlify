const fetch = require('node-fetch').default;
const {Builder, Parser} = require('xml2js');
const constants = require('./constants');

class USPS {
  constructor(userId) {
    this.userId = userId;
  }

  async _callAPI(requestXML) {
    try {
      const uri = constants.apiUrl + requestXML;
      const response = await fetch(uri);
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      const body = await response.text();
      return body;
    }
    catch(ex) {
      throw ex;
    }
  }

  _buildRequestXML(trackingNumbers) {
    let builder = new Builder({explicitRoot: false, headless: true, renderOpts: {pretty: false}});
    let requestObj = {
      TrackFieldRequest: {
        $: {USERID: this.userId},
        Revision: 1,
        ClientIp: '127.0.0.1',
        SourceId: 'SV',
        TrackID: trackingNumbers.map(t => ({$: {ID: t}}))
      }
    };
    let xml = builder.buildObject(requestObj);
    // console.log(xml);
    return xml;
  }

  _parseResponseXML(xml) {
    return new Promise((resolve, reject) => {
      let parser = new Parser();
      parser.parseString(xml, (err, result) => {
        if(err) {
          reject(err);
        }
        else {
          resolve(result);
        }
      });
    });
  }

  _getDeliveryDate(trackingResult) {
    let tracking = trackingResult.$.ID,
      status = trackingResult[constants.statusField][0].toLowerCase(),
      date = 'unknown';
    if(status === constants.statusDelivered.toLowerCase()) {
      date = trackingResult.TrackDetail[0].EventDate[0];
    }
    else if(status === constants.statusOutForDelivery.toLowerCase()) {
      date = trackingResult[constants.predictedDeliveryDate][0];
    }
    else {
      if(trackingResult[constants.expectedDeliveryDate]) {
        date = trackingResult[constants.expectedDeliveryDate][0];
      }
    }
    return {tracking, status, date};
  }

  //This is the 'public' interface
  async getFullTrackingInfo(labels) {
    if(!Array.isArray(labels)) {
      labels = [labels];
    }
    let requestXML = this._buildRequestXML(labels);
    try {
      let response = await this._callAPI(requestXML);
      return this._parseResponseXML(response);
    }
    catch(ex) {
      throw ex;
    }
  }

  async getDeliveryDates(labels) {
    try {
      let result = await this.getFullTrackingInfo(labels);
      // console.log(JSON.stringify(result, null, 2));
      result = result.TrackResponse.TrackInfo.map(t => this._getDeliveryDate(t))
      return result;
    }
    catch(ex) {
      throw ex;
    }
  }
};

module.exports = {USPS};
