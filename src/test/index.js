require('dotenv').config();
const {USPS} = require('../functions/util/usps');

const main = async () => {
  const sampleTracingNumbers = ['9405803699300769787587','9400109205568112003484','9400109205568704082781'];
  const usps = new USPS(process.env.USERID);
  let delivery = await usps.getDeliveryDates(sampleTracingNumbers);
  console.log(delivery);
}

main();
